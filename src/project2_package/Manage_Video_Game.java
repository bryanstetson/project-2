/**
 * 
 */
package project2_package;


import java.util.List;
import java.util.Date;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author Bryan Stetson
 * @author John Mynaugh
 * @date 3/10/14
 *
 */
public class Manage_Video_Game {

	private static SessionFactory factory;
	public static void main(String[] args) {
		try{
			factory = new Configuration().configure().buildSessionFactory();
		}catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		
		Manage_Video_Game MVG = new Manage_Video_Game();
		
		/* Add 20 Video_game records in database */
		Integer vgID1 = MVG.addVideo_game(1, "Final Fantasy X", "PS2", "RPG", "Square");
		Integer vgID2 = MVG.addVideo_game(2, "Mario Party 2", "N64", "Friendship-destroyer", "Hudson Soft");
		Integer vgID3 = MVG.addVideo_game(3, "Skyrim", "PC", "RPG", "Bethesda");
		
		Integer vgID4 = MVG.addVideo_game(4, "Super Mario Bros", "NES", "Side-scrolling, Platformer", "Nintendo");
		Integer vgID5 = MVG.addVideo_game(5, "Final Fantasy VII", "Playstation", "RPG" , "Square");
		Integer vgID6 = MVG.addVideo_game(6, "Guild Wars 2", "PC", "MMORPG", "ArenaNet");
		
		Integer vgID7 = MVG.addVideo_game(7, "World of Warcraft", "PC", "Addiction-setter", "Blizzard Entertainment");
		Integer vgID8 = MVG.addVideo_game(8, "Xenogears", "Playstation", "RPG", "Square");
		Integer vgID9 = MVG.addVideo_game(9, "Chrono Trigger", "Super Nintendo", "RPG", "Square");
		
		Integer vgID10 = MVG.addVideo_game(10, "Megaman 2", "NES", "Action, Platformer", "Capcom");
		Integer vgID11 = MVG.addVideo_game(11, "Final Fantasy Versus XIII", "PS4", "RPG", "Square");
		Integer vgID12 = MVG.addVideo_game(12, "Assassin's Creed II", "PS3", "Action-adventure, Stealth", "Ubisoft");
		
		Integer vgID13 = MVG.addVideo_game(13, "The Legend of Zelda: Ocarina of Time", "N64", "Action-adventure", "Nintendo");
		Integer vgID14 = MVG.addVideo_game(14, "Pokemon Red", "Gameboy", "RPG", "Gamefreak");
		Integer vgID15 = MVG.addVideo_game(15, "GoldenEye 007", "N64", "FPS, Stealth", "Rare");
		
		Integer vgID16 = MVG.addVideo_game(16, "Banjo-Kazooie", "N64", "Platforming, Action-adventure", "Rare");
		Integer vgID17 = MVG.addVideo_game(17, "Super Smash Bros. Melee", "Gamecube", "Fighting", "HAL Laboratory");
		Integer vgID18 = MVG.addVideo_game(18, "Halo 2", "XBox", "FPS", "Bungie Studios");
		
		Integer vgID19 = MVG.addVideo_game(19, "Shadow of the Colossus", "PS2", "Action-adventure", "Team Ico");
		Integer vgID20 = MVG.addVideo_game(20, "Okami", "PS2", "Action-adventure", "Clover Studio");
		
		/* List down all the Video_games */
		MVG.listVideo_games();
		
		/* Display all Video_games on N64 */
		MVG.listN64Video_games();
		
		/* Display all Video_games developed by Square */
		MVG.listSquareVideo_games();
		
		/* Update Video_game's records */
		MVG.updateVideo_game_genre(vgID2, "Party");
		MVG.updateVideo_game_genre(vgID7, "MMORPG");
		MVG.updateVideo_game_name(vgID11, "Final Fantasy XV");
	
		/* Display updated Video_game records */
		MVG.listVideo_games(vgID2);
		MVG.listVideo_games(vgID7);
		MVG.listVideo_games(vgID11);
		
		/* Delete a Video_game from the database */
		MVG.deleteVideo_game(vgID1);
		MVG.deleteVideo_game(vgID2);
		MVG.deleteVideo_game(vgID3);
		MVG.deleteVideo_game(vgID4);
		MVG.deleteVideo_game(vgID5);
		MVG.deleteVideo_game(vgID6);
		MVG.deleteVideo_game(vgID7);
		MVG.deleteVideo_game(vgID8);
		MVG.deleteVideo_game(vgID9);
		MVG.deleteVideo_game(vgID10);
		MVG.deleteVideo_game(vgID11);
		MVG.deleteVideo_game(vgID12);
		MVG.deleteVideo_game(vgID13);
		MVG.deleteVideo_game(vgID14);
		MVG.deleteVideo_game(vgID15);
		MVG.deleteVideo_game(vgID16);
		MVG.deleteVideo_game(vgID17);
		MVG.deleteVideo_game(vgID18);
		MVG.deleteVideo_game(vgID19);
		MVG.deleteVideo_game(vgID20);

	}
		
	/* Method to CREATE a Video_game in the database */
	public Integer addVideo_game(int Video_game_id, String Video_game_name, String System, String Genre, String Developer){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		Video_game Video_game = new Video_game(Video_game_id, Video_game_name, System, Genre, Developer);
		Video_game_id = (Integer) session.save(Video_game);
		tx.commit();
		}catch (HibernateException e) {
		if (tx!=null) tx.rollback();
		e.printStackTrace();
		}finally {
		session.close();
		}
		return Video_game_id;
	}
	
	/* Method to READ all the Video_games */
	public void listVideo_games( ){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		List Video_games = session.createQuery("FROM Video_game").list();
		for (Iterator iterator = Video_games.iterator(); iterator.hasNext();){
			Video_game Video_game = (Video_game) iterator.next();
			System.out.print("ID: " + Video_game.getVideo_game_id());
			System.out.print(" Name: " + Video_game.getVideo_game_name());
			System.out.print(" System: " + Video_game.getSystem());
			System.out.print(" Genre: " + Video_game.getGenre());
			System.out.println(" Developer: " + Video_game.getDeveloper());
		}
		
		tx.commit();
		
		}catch (HibernateException e) {
		if (tx!=null) tx.rollback();
			e.printStackTrace();
		}finally {
			session.close();
		}
	}
	
	/* Method to READ all the Video_games on N64*/
	public void listN64Video_games( ){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		List Video_games = session.createQuery("FROM Video_game").list();
		for (Iterator iterator = Video_games.iterator(); iterator.hasNext();){
			Video_game Video_game = (Video_game) iterator.next();
			if(Video_game.getSystem().equals("N64")){
				System.out.print("ID: " + Video_game.getVideo_game_id());
				System.out.print(" Name: " + Video_game.getVideo_game_name());
				System.out.print(" System: " + Video_game.getSystem());
				System.out.print(" Genre: " + Video_game.getGenre());
				System.out.println(" Developer: " + Video_game.getDeveloper());}
		}
		
		tx.commit();
		
		}catch (HibernateException e) {
		if (tx!=null) tx.rollback();
			e.printStackTrace();
		}finally {
			session.close();
		}
	}
	
	/* Method to READ all the Video_games developed by Square*/
	public void listSquareVideo_games( ){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		List Video_games = session.createQuery("FROM Video_game").list();
		for (Iterator iterator = Video_games.iterator(); iterator.hasNext();){
			Video_game Video_game = (Video_game) iterator.next();
			if(Video_game.getDeveloper().equals("Square")){
				System.out.print("ID: " + Video_game.getVideo_game_id());
				System.out.print(" Name: " + Video_game.getVideo_game_name());
				System.out.print(" System: " + Video_game.getSystem());
				System.out.print(" Genre: " + Video_game.getGenre());
				System.out.println(" Developer: " + Video_game.getDeveloper());}
		}
		
		tx.commit();
		
		}catch (HibernateException e) {
		if (tx!=null) tx.rollback();
			e.printStackTrace();
		}finally {
			session.close();
		}
	}
	
	/* Method to READ the Video_game with given video_game_id*/
	public void listVideo_games(int video_game_id){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		List Video_games = session.createQuery("FROM Video_game").list();
		for (Iterator iterator = Video_games.iterator(); iterator.hasNext();){
			Video_game Video_game = (Video_game) iterator.next();
			if(Video_game.getVideo_game_id() == video_game_id){
				System.out.print("ID: " + Video_game.getVideo_game_id());
				System.out.print(" Name: " + Video_game.getVideo_game_name());
				System.out.print(" System: " + Video_game.getSystem());
				System.out.print(" Genre: " + Video_game.getGenre());
				System.out.println(" Developer: " + Video_game.getDeveloper());}
		}
		
		tx.commit();
		
		}catch (HibernateException e) {
		if (tx!=null) tx.rollback();
			e.printStackTrace();
		}finally {
			session.close();
		}
	}
	
	/* Method to UPDATE genre for a Video_game */
	public void updateVideo_game_genre(Integer Video_game_id, String genre ){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		Video_game Video_game = (Video_game)session.get(Video_game.class, Video_game_id);
		Video_game.setGenre( genre );
		session.update(Video_game);
		tx.commit();
		
		}catch (HibernateException e) {
		if (tx!=null) tx.rollback();
			e.printStackTrace();
		}finally {
			session.close();
		}
	}
	
	/* Method to UPDATE Video_game_name for a Video_game */
	public void updateVideo_game_name(Integer Video_game_id, String name ){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
		tx = session.beginTransaction();
		Video_game Video_game = (Video_game)session.get(Video_game.class, Video_game_id);
		Video_game.setVideo_game_name( name );
		session.update(Video_game);
		tx.commit();
		
		}catch (HibernateException e) {
		if (tx!=null) tx.rollback();
			e.printStackTrace();
		}finally {
			session.close();
		}
	}
	
	/* Method to DELETE a Video_game from the records */
	public void deleteVideo_game(Integer Video_game_id){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			Video_game Video_game = (Video_game)session.get(Video_game.class, Video_game_id);
			session.delete(Video_game);
			tx.commit();
		}catch (HibernateException e) {
			if (tx!=null) tx.rollback();
				e.printStackTrace();
			}finally {
				session.close();
		}
	}
}