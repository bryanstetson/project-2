package project2_package;

public class Video_game {
	private int Video_game_id;
	private String Video_game_name;
	private String System;
	private String Genre;
	private String Developer;
	
	//Constructors Default needed for hibernate
	public Video_game() {}	
	public Video_game(int Video_game_id, String Video_game_name, 
			String System, String Genre, String Developer){
		this.Video_game_id = Video_game_id;
		this.Video_game_name = Video_game_name;
		this.System = System;
		this.Genre = Genre;
		this.Developer = Developer;
	}
	
	public int getVideo_game_id(){
		return Video_game_id;
	}
	public void setVideo_game_id(int Id){
		this.Video_game_id = Id;
	}
	public String getVideo_game_name(){
		return Video_game_name;
	}
	public void setVideo_game_name(String Name){
		this.Video_game_name = Name;
	}
	public String getSystem(){
		return System;
	}
	public void setSystem(String System){
		this.System = System;
	}
	public String getGenre(){
		return Genre;
	}
	public void setGenre(String Genre){
		this.Genre = Genre;
	}
	public String getDeveloper(){
		return Developer;
	}
	public void setDeveloper(String Developer){
		this.Developer = Developer;
	}
}
